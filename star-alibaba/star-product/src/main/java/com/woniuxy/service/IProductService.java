package com.woniuxy.service;

import com.woniuxy.domain.Product;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:45:41
 */
public interface IProductService {
    Product findByPid(Integer pid);
    void deductStock(Integer pid, Integer number);
}
