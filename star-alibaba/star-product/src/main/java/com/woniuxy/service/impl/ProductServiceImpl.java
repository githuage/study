package com.woniuxy.service.impl;

import com.woniuxy.dao.IProductDao;
import com.woniuxy.domain.Product;
import com.woniuxy.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:45:53
 */
@Service
@Transactional
public class ProductServiceImpl implements IProductService {
    @Autowired
    private IProductDao dao;

    public Product findByPid(Integer pid) {
        Optional<Product> optional = dao.findById(pid);
        return optional.get();
    }

    @Override
    public void deductStock(Integer pid, Integer number) {
        Product product  = dao.findById(pid).get();
        product.setStock(product.getStock() - number);
        dao.save(product);
    }
}
