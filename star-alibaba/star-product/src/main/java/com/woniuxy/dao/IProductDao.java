package com.woniuxy.dao;

import com.woniuxy.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:46:13
 */
public interface IProductDao extends JpaRepository<Product, Integer> {
}
