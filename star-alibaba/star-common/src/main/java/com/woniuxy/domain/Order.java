package com.woniuxy.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:17:06
 */
@Entity(name = "star_order")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long oid;
    private Integer uid;
    private String username;

    private Integer pid;
    private String pname;
    private Double price;

    private Integer number; // 购买数量
}