package com.woniuxy.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:14:35
 */
@Entity(name = "star_user")
@Data
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;

    private String username;
    private String password;
    private String telephone;
}

/*
create table 表名
(
  列名 类型 auto_increment
  列名 类型，
  列名 类型，
  列名 类型
  primary key(列名)
);

 */
