package com.woniuxy.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:18:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVO implements Serializable {
    private Integer status;     // 响应码
    private String message;     // 响应简要信息
    private Object data;        // 响应的主体数据

    public ResultVO(Integer status, String message) {
        this.status = status;
        this.message = message;
    }
}