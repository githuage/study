package com.woniuxy.util;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:19:18
 */
public final class Constants {
    public static final int OPER_SUCCESS = 200;
    public static final int OPER_FAILURE = 500;
}
