package com.woniuxy.staralibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarAlibabaApplication {

    public static void main(String[] args) {
        SpringApplication.run(StarAlibabaApplication.class, args);
    }

}
