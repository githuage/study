package com.woniuxy.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/9 14:15:16
 */
@Service
public class OrderServiceImpl2 {

    // 以下的message方法，就是我们想要保护资源
    // 要把一个方法定义为sentinel保护的资源，必须添加一个注解
    @SentinelResource("servicemessage")
    public String message() {
        return "message";
    }

}
