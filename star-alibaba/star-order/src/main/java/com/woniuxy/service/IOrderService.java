package com.woniuxy.service;

import com.woniuxy.domain.Order;
import io.seata.spring.annotation.GlobalLock;
import io.seata.spring.annotation.GlobalTransactional;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:49:48
 */
public interface IOrderService {

    void create(Order order);
}
