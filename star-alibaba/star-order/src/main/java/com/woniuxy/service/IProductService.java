package com.woniuxy.service;

import com.woniuxy.util.ResultVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/9 09:50:35
 */
/* 以下注解的作用是，告诉当前微服务，该接口是一个假接口
 该假接口不负责真正的业务处理，而是进行远程调用。
 同时，spring ioc容器还会利用jdk动态代理，动态地创建出该接口的实现类。
 在把该实现类的实例存入spring ioc容器中。

 远程调用时。必须知道对方服务的套接字，以及对方的端点

 套接字已经有了，就是product-service，openfeign这个框架，可以从nacos中
 根据product-service服务名轮询地获取到具体的ip地址和端口号，至于端点是
 什么，就看我们调用该接口的哪个方法了，因为在方法上的注解，就封装了端点！
 */

@FeignClient("product-service")
public interface IProductService {

    @GetMapping("products/{pid}")
    ResultVO findByPid(@PathVariable("pid") Integer pid);

    @GetMapping("products/deductStock/{pid}/{number}")
    ResultVO deductStock(@PathVariable("pid") Integer pid, @PathVariable("number") Integer number);

    @GetMapping("products/testloadbalance")
    ResultVO testLoadBalance();
}
