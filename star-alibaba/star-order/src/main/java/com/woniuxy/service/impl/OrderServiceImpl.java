package com.woniuxy.service.impl;

import com.woniuxy.dao.IOrderDao;
import com.woniuxy.domain.Order;
import com.woniuxy.service.IOrderService;
import com.woniuxy.service.IProductService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:50:01
 */
@Service
@Transactional

public class OrderServiceImpl implements IOrderService {
    @Autowired
    private IOrderDao dao;
    @Autowired
    private IProductService productService;
    @GlobalTransactional
    public void create(Order order) {
        dao.save(order);
        productService.deductStock(order.getPid(),order.getNumber());

    }

}
