package com.woniuxy.config;

import com.alibaba.csp.sentinel.adapter.servlet.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/9 15:47:52
 */

// 该类的主要作用，就是用来获取访问资源的上级微服务的标识。
@Component
public class RequestOriginParserDefinition implements RequestOriginParser {
    public String parseOrigin(HttpServletRequest request) {
        // 该方法所返回的值，就会被认为是调用当前资源的另外一个微服务的标识
        String from = request.getParameter("from");
        return from;
    }
}
