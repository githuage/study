package com.woniuxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:48:34
 */
// @SpringBootApplication中封装了@Configuration注解
// 所以当前的启动类，也是一个配置！其中的@Bean会被spring ioc容器管理起来！
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OrderApp {
    public static void main(String[] args) {
        SpringApplication.run(OrderApp.class, args);
    }

    @Bean
    // 只要加上这个注解，那么容器中的RestTemplate实例，就天生具备负载均衡的功能了！
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }


}
