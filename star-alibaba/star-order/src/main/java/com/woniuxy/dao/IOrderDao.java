package com.woniuxy.dao;

import com.woniuxy.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:50:18
 */
public interface IOrderDao extends JpaRepository<Order, Integer> {
}
