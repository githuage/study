package com.woniuxy.dao;

import com.woniuxy.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author gaopeng
 * @create 2020/04/2020/4/8 10:41:48
 */
public interface IUserDao extends JpaRepository<User, Integer> {
}
