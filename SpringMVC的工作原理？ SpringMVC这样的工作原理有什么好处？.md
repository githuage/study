SpringleMVC的工作原理

      1、客户端请求前端控制器

      2、前端控制器将请求发送给适配器控制器

      3、适配器控制器找到对应的handler发送给前端控制器

      4、前端控制器将handler发送给映射器控制器

      5、映射器控制器返回ModelAndView

      6、前端控制器将	ModelAndView发送给视图解析器

      7、视图解析器解析完成返回页面和数据

      8、前端控制器将页面和数据返回给客户端浏览器

     


![输入图片说明](https://images.gitee.com/uploads/images/2020/0504/194727_d4b01801_7359235.png "屏幕截图.png") 
优点
（1）可以支持各种视图技术,而不仅仅局限于JSP；

（2）与Spring框架集成（如IoC容器、AOP等）；

（3）清晰的角色分配：前端控制器(dispatcherServlet) , 请求到处理器映射（handlerMapping), 处理器适配器（HandlerAdapter), 视图解析器（ViewResolver）。

（4） 支持各种请求资源的映射策略。



