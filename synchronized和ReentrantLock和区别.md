## 1、原始构成

​     synchronized是关键属天JVM层面，monitorenter（底层是通过monitor对系来完成，其实wait/notify 等方法也依赖monitor对象只有在同步块或方法中才能调wait/notify等方法 monitorexit
 Lock是具体类（java.util.concurrent.Locks.Lock）是api层面的锁

## 2、使用方法

​     synchronized 不需要用户去手动释放馈，当synchronized代码执行完后系统会自动让线程释放对衡的占用
​     ReentrantLock则需要用户去手动释放锁若没有主动释放敛，就有可能导致出现死锁现象。
​     需要lock（）unLock（）方法配合try/finally语句块来完成。

## 3、等待是否可中断

​    synchronized不可中断，除非抛出异常或者正常运行完成
​    ReentrantLock 可中断，1.设置超时方法 trylock（long timeout，Timeunit unit）

​                         						2.LockInterruptibly（）放代码块中，调用interrupt（）方法可中断

```java
package com.woniuxy.g_lock.b;

import java.io.IOException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**

- @auther: 秦浦华
- @DATE: 2020/5/4   10:40
  */
  public class Test {
  public static void main(String[] args) throws IOException, InterruptedException {
    	TestDemo t1 = new TestDemo();
    	Thread task = new Thread(t1, "张三");
    	Thread task2 = new Thread(t1, "李四");
    	task.start();
    	Thread.sleep(266); //保证task拿到锁

		task2.start();
		System.in.read();   //main阻塞
		task2.interrupt();   //中断线程等待
      }
}

class  TestDemo implements Runnable {
Lock lock = new ReentrantLock();
long startTime = System.currentTimeMillis();

@Override
public void run() {
    try {
        System.out.println(Thread.currentThread().getName()+"开始执行任务");
        lock.lockInterruptibly();
        System.out.println(Thread.currentThread().getName()+"获得了锁");
        while (true) {
            if ( System.currentTimeMillis()-startTime>Integer.MAX_VALUE){
                break;
            }
        }
   		 System.out.println(Thread.currentThread().getName()+"结束执行任务");
		} catch (InterruptedException e) {
    		e.printStackTrace();

		}finally {
   			 try {
	
       			 lock.unlock();

    			}catch (Exception e){

   		 }
   		 System.out.println(Thread.currentThread().getName()+"结束等待去做其他事");
		}
 	}
}
```

```java
package com.woniuxy.g_lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther: 秦浦华
 * @DATE: 2020/5/4   11:15
 */
public class TryLockDemo {
    //实例化Lock对象
    Lock lock = new ReentrantLock();

    /**
     * @param args
     */
    public static void main(String[] args) {
        //实例化本类对象，目的是调用runThread方法
        TryLockDemo tl = new TryLockDemo();
        //匿名对象创建线程1，并重写run方法，启动线程

        new Thread(()  ->{

            tl.runThread(Thread.currentThread());
        },"A").start();
        //匿名对象创建线程2，并重写run方法，启动线程
        new Thread(()  ->{

            tl.runThread(Thread.currentThread());
        },"B").start();


    }
    //线程共同调用方法
    public void runThread(Thread t){
        //lock对象调用trylock()方法尝试获取锁
        if(lock.tryLock()){
            //获锁成功代码段
            System.out.println("线程"+t.getName()+"获取锁成功");
            try {
                //执行的代码
                Thread.sleep(5000);
            } catch (Exception e) {
                //异常处理内容，比如中断异常，需要恢复等
            } finally {
                //获取锁成功之后，一定记住加finally并unlock()方法,释放锁
                System.out.println("线程"+t.getName()+"释放锁");
                lock.unlock();
            }
        }else{
            //获锁失败代码段
            //具体获取锁失败的回复响应
            System.out.println("线程"+t.getName()+"获取锁失败");
        }
    }
}

```





tryLock(long time 等待的时间, TimeUnit unit等待时间的单位)方法和tryLock()方法是类似的，只不过区别在于这个方法在拿不到锁时会等待一定的时间，在时间期限之内如果还拿不到锁，就返回false。如果如果一开始拿到锁或者在等待期间内拿到了锁，则返回true。

## 4、加锁是否公平

synchronized非公平锁

ReentrantLock两者都可以，默认非公平锁，构造方法可以传入boolean值，true为公平锁，false为非公平锁

```java
package com.woniuxy.g_lock.d;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther: 秦浦华
 * @DATE: 2020/5/4   11:52
 */
public class QPHTest {
    public static void main(String[] args) throws InterruptedException {
        FairAndUnfair fairAndUnfair = new FairAndUnfair();
        Thread t1 = new Thread(fairAndUnfair,"a");
        Thread t2 = new Thread(fairAndUnfair,"b");
        t1.start();
        Thread.sleep(2);
        t2.start();
    }
}
class FairAndUnfair implements  Runnable{
    Lock lock = new ReentrantLock(true);

    @Override
    public void run() {


            for (int i = 1; i<10;i++){
                lock.lock();
                System.out.println(Thread.currentThread().getName()+"来了");
                try {
                    Thread.sleep(32);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                lock.unlock();
            }

    }
}
```



## 

## 5、绑定多个条件condition 

synchronized没有

ReentrantLock用来实现分组唤醒需要唤醒的线程们，可以精确唤醒，而不是像synchronized要么随机唤醒一个线程要么唤醒全部线程。

```java
package com.woniuxy.g_lock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther: 秦浦华
 * @DATE: 2020/5/3   15:08
 */
public class lock {

    public static void main(String[] args) {
        shiro shiro = new shiro();


      new Thread(()  ->{
          for (int i =0;i<10;i++){
              shiro.print5();
          }

        },"A").start();

        new Thread(()  ->{
            for (int i =0;i<10;i++){
                shiro.print10();
            }
        },"B").start();

        new Thread(()  ->{


            for (int i =0;i<10;i++){
                shiro.print15();
            }

        },"C").start();
    }

}
class shiro{
    private int number =1;  //1:A  2:B   3:C

    Lock lock = new ReentrantLock();
    Condition c1 = lock.newCondition();
    Condition c2 = lock.newCondition();
    Condition c3 = lock.newCondition();

    public  void print5(){
        lock.lock();

        try {
            while (number!=1){
                c1.await();
            }
            for (int i =1;i<=5;i++){
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            number=2;
            c2.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public  void print10(){
        lock.lock();

        try {
            while (number!=2){
                c2.await();
            }
            for (int i =1;i<=10;i++){
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            number=3;
            c3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public  void print15(){
        lock.lock();

        try {
            while (number!=3){
                c3.await();
            }
            for (int i =1;i<=15;i++){
                System.out.println(Thread.currentThread().getName()+"\t"+i);
            }
            number=1;
            c1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
```



## 6、抛出异常时，synchronized会释放锁    lock不会

```java
package com.woniuxy.g_lock.c;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther: 秦浦华
 * @DATE: 2020/5/4   11:24
 */
public class QphTest {
    public static void main(String[] args) {
        LockDemo lockDemo = new LockDemo();

        Thread t1 = new Thread(lockDemo,"张三");
        Thread t2 = new Thread(lockDemo,"李四");
        t1.start();
        t2.start();
    }


}
class LockDemo implements Runnable{
    Lock lock = new ReentrantLock();
    @Override
    public void run() {

        try {
            //lock.lock();
            synchronized (this) {
                for (int i = 1; i < 100; i++) {
                    if (i == 10) {
                        throw new RuntimeException();
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "第" + i + "次开始执行任务");
                }
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }finally{

        }

    }
}
```



二者都是可重入锁：
    可重入锁（也叫做递归锁）指的是同一线程外层函数获得锁之后，内层递归函数仍然能获取该锁的代码，在同一个线程在外层方法获取锁的时候，在进入内层方法会自动获取锁也即是说，线程可以进入任何一个它已经拥有的锁所同步着的代码块。

补充：
公平锁：
    是指多个线程按照申请锁的顺序来获取锁类似排队打饭 先来后到
非公平锁：
    是指在多线程获取锁的顺序并不是按照申请锁的顺序,有可能后申请的线程比先申请的线程优先获取到锁,在高并发的情况下,有可能造成优先级反转或者饥饿现象
自旋锁：
    是指尝试获取锁的线程不会立即阻塞，而是采用循环的方式去尝试获取锁，这样的好处是减少线程上下文切换的消耗，缺点是循环会消耗CPU

独占锁：
指该锁一次只能被一个线程所持有。对ReentrantLock和Synchronized而言都是独占锁
共享锁：指该锁可被多个线程所持有。
对ReentrantReadWriteLock其读锁是共享锁，其写锁是独占锁。
读锁的共享锁可保证并发读是非常高效的，读写，写读，写写的过程是互斥的。