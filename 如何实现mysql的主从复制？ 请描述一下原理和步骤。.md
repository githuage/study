1、修改主机的配置文件：（/etc/my.cnf）
在[mysqld]下加入以下配置

```properties
# 主服务器唯一ID
server-id=1
# 启用二进制日志
log-bin=mysql-bin
# 设置不需要复制的数据库（可设置多个）
binlog-ignore-db=mysql
binlog-ignore-db=information_schema
# 设置需要复制的数据库名字
binlog-do-db=testdb
# 设置logbin格式
binlog_format=STATEMENT
```
binlog日志的三种格式

1. statement，记录“写sql”
2. row，只记录每一行的改变
3. mixed，混合模式



修改从机（54）的配置文件：(/etc/my.cnf)

在[mysqld]下加入以下配置

```properties
# 从服务器唯一ID
server-id=2
# 启用中继日志
relay-log=mysql-relay
```



然后重启主机（53）和从机（54）

```bash
systemctl restart mysqld
```



主机从机都开放3306端口（或者关闭防火墙）



在主机里执行授权命令，账户名为：slave，密码是：123

```sql
GRANT REPLICATION SLAVE ON *.* TO 'slave'@'%' IDENTIFIED BY '123';
```



然后查询master的状态

![image-20200425211316154](mycat.assets/image-20200425211316154.png)

记录下File和Position的值，这两个值，就用在下面，从机中使用。

<font color="red">执行完以上步骤后，不要再操作主机了，防止主机服务状态值变化</font>。







在从机上，需要先执行一下以下的命令：

```sql
stop slave;
```



然后，在从机上，执行以下命令，以让从机去复制主机的数据（注意，以下的2个配置用到了主机中的File和Position）

```sql
change master to
master_host='192.168.1.53',
master_user='slave',
master_password='123',
master_log_file='mysql-bin.000001',
master_log_pos=518;
```



![image-20200425220055672](mycat.assets/image-20200425220055672.png)



再在从机上执行以下命令

```sql
start slave;
```



再查看从机状态：

```sql
show slave status;
```



![image-20200425220630419](mycat.assets/image-20200425220630419.png)

注意，Slave_IO_Running和Slave_SQL_Running取值都为Yes，就表示主从复制搭建成功了。如果其中有一个是No，则应该查看下面的Error报错信息。进而根据报错信息再进行调试。



测试，在主机上创建testdb数据库，并且创建user表，再插入数据，在从机上也能看到相同的数据！

```sql
CREATE DATABASE testdb;

CREATE TABLE USER
(
  id INT PRIMARY KEY AUTO_INCREMENT,
  NAME VARCHAR(20),
  birthdate DATE,
  balance DOUBLE
);

INSERT INTO USER VALUES(NULL, 'andy', NOW(), 1000);

SELECT * FROM USER;
```

至此，mysql主从复制环境搭建完毕。将来任何时候想要停止主从复制关系的话，只要在从机这一方键入stop slave命令即可。

