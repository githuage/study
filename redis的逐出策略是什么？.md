noeviction：当内存使用达到阈值的时候，所有引起申请内存的命令会报错。
allkeys-lru：在主键空间中，优先移除最近未使用的key。
volatile-lru：在设置了过期时间的键空间中，优先移除最近未使用的key。
allkeys-random：在主键空间中，随机移除某个key。
volatile-random：在设置了过期时间的键空间中，随机移除某个key。
volatile-ttl：在设置了过期时间的键空间中，具有更早过期时间的key优先移除。
volatile-* 系列指令在无键值满足条件时（例如未设置过期时间），表现为 noeviction
```
实现
    客户端执行一个命令, 导致 Redis 中的数据增加,占用更多内存。
    Redis 检查内存使用量, 如果超出 maxmemory 限制, 根据策略清除部分 key。
    继续执行下一条命令, 以此类推。
```
在这个过程中, 内存使用量会不断地达到 limit 值, 然后超过, 然后删除部分 key, 使用量又下降到 limit 值之下。

如果某个命令导致大量内存占用(比如通过新key保存一个很大的set), 在一段时间内, 可能内存的使用量会明显超过 maxmemory 限制。

maxmemory 配置指令

maxmemory 用于指定 Redis 能使用的最大内存。既可以在 redis.conf 文件中设置, 也可以在运行过程中通过 CONFIG SET 命令动态修改。

例如, 要设置 100MB 的内存限制, 可以在 redis.conf 文件中这样配置：
```
maxmemory 100mb
```
    1

将 maxmemory 设置为 0, 则表示不进行内存限制。当然, 对32位系统来说有一个隐性的限制条件: 最多 3GB 内存。

当内存使用达到最大限制时, 如果需要存储新数据, 根据配置的策略(policies)的不同, Redis可能直接返回错误信息, 或者删除部分老的数据。
