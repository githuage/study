不同点：

1、JDK1.7用的是头插法，而JDK1.8及之后使用的都是尾插法.
2、JDK1.7是底层是数组+链表而JDK1.8是数组+链表+红黑树
3、扩容时计算存储位置的算法
```
    a、在JDK1.7的时候是直接用hash值和需要扩容的二进制数进行&（这里就是为什么扩容的时候为啥一定必须是2的多少次幂的原因所在，
 因为如果只有2的n次幂的情况时最后一位二进制数才一定是1，这样能最大程度减少hash碰撞）（hash值 & length-1）

   b、而在JDK1.8的时候直接用了JDK1.7的时候计算的规律，也就是扩容前的原始位置+扩容的大小值=JDK1.8的计算方式，
而不再是JDK1.7的那种异或的方法。但是这种方式就相当于只需要判断Hash值的新增参与运算的位是0还是1就直接迅速计算出了扩容后的储存方式。
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/094404_0e38f6cd_7359235.png "屏幕截图.png")

哈希扩容：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/094452_cb71017f_7359235.png "屏幕截图.png")

为什么HashMap具备下述特点：键-值（key-value）都允许为空、线程不安全、不保证有序、存储位置随时间变化？
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/094517_d1024e2e_7359235.png "屏幕截图.png")
为什么 HashMap 中 String、Integer 这样的包装类适合作为 key 键
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/094716_1cb1b91e_7359235.png "屏幕截图.png")
HashMap 中的 key若 Object类型， 则需实现哪些方法？
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/094740_e34e02a5_7359235.png "屏幕截图.png")